<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if ( Auth::check() ) {
		return view('welcome');	
	} else {
		return redirect('login');
	}
});

Auth::routes();

// admin routes
Route::get('admin/home', 'HomeController@index')->name('home');
Route::get('member/home', 'HomeController@index')->name('member');

// settings
Route::get('settings', 'SettingsController@index')->name('settings');
Route::post('settings/profile', 'SettingsController@profile_update')->name('profile.update');
Route::post('settings/password', 'SettingsController@password_update')->name('password.update');

// Restful Controllers
Route::resources([
	'users' => 'UsersController',
	'roles' => 'RolesController',
]);

