<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_admin = Role::where('name', 'admin')->first();
    	$role_member = Role::where('name', 'member')->first();

    	$admin = new User();
        $admin->name = 'Administrator';
        $admin->first_name = 'Peter';
    	$admin->last_name = 'Porras';
    	$admin->email = 'admin@admin.com';
    	$admin->password = bcrypt('secret');
    	$admin->save();

    	$admin->roles()->attach($role_admin);

    	$member = new User();
    	$member->name = 'Member';
        $member->first_name = 'Peter';
        $member->last_name = 'Porras';
    	$member->email = 'member@member.com';
    	$member->password = bcrypt('secret');
    	$member->save();
    	$member->roles()->attach($role_member);
    }
}
