@extends('layouts.public')

@section('content')

<div class="middle-box text-center loginscreen  animated fadeInDown">

    <div>
        <div>
            <h1 class="logo-name" style="font-size: 100px;">LOGIN</h1>
        </div>
        <form class="m-t" method="post" role="form" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input name="email" value="{{ old('email') }}" type="email" class="form-control" placeholder="Email" required="">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input name="password" type="password" class="form-control" placeholder="Password" required="">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                    </label>
                </div>
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

            <a href="{{ route('password.request') }}"><small>Forgot password?</small></a>
            {{-- <p class="text-muted text-center"><small>Do not have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a> --}}
        </form>
        <p class="m-t"> <small>&copy; {{ date('Y') }}</small> </p>
    </div>

</div>

@endsection


