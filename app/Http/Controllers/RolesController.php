<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Role;
use Auth;

class RolesController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Roles',
            'description' => 'Manage all user roles.',
            'roles' => Role::all(),
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->params['roles'] = Role::paginate(10);

        return view('roles.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->params['title'] = 'Register new user role';
        $this->params['description'] = 'Customize and register new user role.';

        return view('roles.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles('admin');

        $validate = array(
            'name' => 'required|string|max:255|unique:roles',
            'description' => 'required|string|max:255',
        );

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('roles/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $role = new Role();
        $role->fill( $request->all() );
        $role->save();

        return redirect('roles')->with('success', 'Role ' . $role->name . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['title'] = 'Edit Role';
        $this->params['description'] = 'Update role information.';

        $role = Role::find( $id );

        if ( ! $role ) {
            return redirect('roles')->with('warning', 'Role no longer exist.');
        }

        $this->params['role'] = $role;

        return view('roles.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Block users who are not admin
        $request->user()->authorizeRoles('admin');

        $validate = array(
            'description' => 'required|string|max:255',
        );

        $role = Role::find( $id );

        // double check if user exist.
        if ( ! $role ) {
            return redirect('roles')->with('error', 'User role does not exist, please try again.');
        }

        if ( $role->name != $request->input('name') ) {
            $validate['name'] = 'required|string|max:255|unique:roles';
        }

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('roles/'. $role->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $role->fill( $request->all() );
        $role->save();

        return redirect('roles/'. $role->id .'/edit')->with('success', 'User role details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        // Block users who are not admin
        $request->user()->authorizeRoles('admin');

        $role = Role::find( $id );

        if ( ! $role ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        }

        $role->delete();

        return response()->json([
            'error' => false,
            'message' => 'User role successfuly removed.'
        ]);
    }
}
